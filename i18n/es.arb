{
  "@@last_modified": "2022-04-24T15:30:46.032668",
  "@@locale": "es",
  "General": "General",
  "@General": {
    "description": "Used as a header in the config screen to refer to \"general settings\"",
    "type": "text",
    "placeholders": {}
  },
  "Continue": "Continuar",
  "@Continue": {
    "description": "Used as a button in the 'first time' configuration screen",
    "type": "text",
    "placeholders": {}
  },
  "Use neutral language": "Usar lenguaje neutral",
  "@Use neutral language": {
    "description": "Toggle button that enables (or disables) use of \"headache\" instead of \"migraine\"",
    "type": "text",
    "placeholders": {}
  },
  "Use the term \"headache\" rather than \"migraine\"": "Usar el término \"dolor de cabeza\" en lugar de \"migraña\"",
  "@Use the term \"headache\" rather than \"migraine\"": {
    "type": "text",
    "placeholders": {}
  },
  "What kind of headache do you have?": "¿Qué tipo de dolor de cabeza tiene?",
  "@What kind of headache do you have?": {
    "type": "text",
    "placeholders": {}
  },
  "Select this if you're unsure": "Seleccione esta opción si no está seguro",
  "@Select this if you're unsure": {
    "type": "text",
    "placeholders": {}
  },
  "Migraine": "Migraña",
  "@Migraine": {
    "description": "Used as level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Other (general headaches, cluster etc.)": "Otros (cefaleas generales, en racimo, etc.)",
  "@Other (general headaches, cluster etc.)": {
    "type": "text",
    "placeholders": {}
  },
  "Settings": "Ajustes",
  "@Settings": {
    "type": "text",
    "placeholders": {}
  },
  "Configuration": "Configuración",
  "@Configuration": {
    "description": "The name of the first time configuration screen. This is pretty much like the settings screen, but with some different information and displayed only once, therefore using a different name to make this clearer.",
    "type": "text",
    "placeholders": {}
  },
  "Medication list": "Lista de medicamentos",
  "@Medication list": {
    "description": "Header for the part of the configuration screen where the user sets up their medication",
    "type": "text",
    "placeholders": {}
  },
  "Changing the list of medications here will not change medications in entries you have already added.": "El cambio de la lista de medicamentos aquí no cambiará los medicamentos en las entradas que ya ha añadido.",
  "@Changing the list of medications here will not change medications in entries you have already added.": {
    "type": "text",
    "placeholders": {}
  },
  "If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": "Si no desea añadir sus medicamentos, sólo tiene que pulsar \"Continuar\" y el Registro de migraña añadirá dos genéricos para que los utilice.",
  "@If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": {
    "type": "text",
    "placeholders": {}
  },
  "Add a new medication": "Añadir nuevo medicamento",
  "@Add a new medication": {
    "type": "text",
    "placeholders": {}
  },
  "Undo": "Deshacer",
  "@Undo": {
    "type": "text",
    "placeholders": {}
  },
  "_deletedElement": "Borrar \"{medication}\"",
  "@_deletedElement": {
    "description": "A pop-up message after the user has deleted a medication. Is accompanied with an 'undo' button.",
    "type": "text",
    "placeholders": {
      "medication": {}
    }
  },
  "_andMessage": "{med1} y {med2}",
  "@_andMessage": {
    "description": "Used to join two (or more) medications together. med1 is either a single medication, or a comma-separated list of medications. med2 is always just a single medication. Neither can be empty/null.",
    "type": "text",
    "placeholders": {
      "med1": {},
      "med2": {}
    }
  },
  "No registration": "Sin registro",
  "@No registration": {
    "description": "Used in the pie diagram and table on the front page to denote days where no entries have been added. Avoid terminology like 'no headaches', since that might not be quite correct.",
    "type": "text",
    "placeholders": {}
  },
  "Headache": "Dolor de cabeza",
  "@Headache": {
    "description": "Used as level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Mild headache": "Dolor de cabeza leve",
  "@Mild headache": {
    "description": "Used as neutral level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Moderate headache": "Dolor de cabeza moderado",
  "@Moderate headache": {
    "description": "Used as neutral level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Strong migraine": "Migraña intenso",
  "@Strong migraine": {
    "description": "Used as level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Strong headache": "Dolor de cabeza intenso",
  "@Strong headache": {
    "description": "Used as neutral level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Migraine medication": "Medicación para la migraña",
  "@Migraine medication": {
    "description": "Used as a generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders": {}
  },
  "Seizure medication": "Medicación anticonvulsiva",
  "@Seizure medication": {
    "description": "Used as neutral generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders": {}
  },
  "Painkillers": "Analgésicos",
  "@Painkillers": {
    "description": "Used as the second 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders": {}
  },
  "Edit": "Editar",
  "@Edit": {
    "type": "text",
    "placeholders": {}
  },
  "Register": "Registrar",
  "@Register": {
    "description": "Displayed as the header when registering a new migraine attack",
    "type": "text",
    "placeholders": {}
  },
  "You must select a strength": "Debe seleccionar una intensidad",
  "@You must select a strength": {
    "description": "Notice that appears if the user tries to save an entry without selecting a strength",
    "type": "text",
    "placeholders": {}
  },
  "Save": "Guardar",
  "@Save": {
    "type": "text",
    "placeholders": {}
  },
  "editingEntryNewDate": "Está editando una entrada en {editDate}. Si se cambia la fecha, esta entrada se moverá a la nueva fecha.",
  "@editingEntryNewDate": {
    "type": "text",
    "placeholders": {
      "editDate": {}
    }
  },
  "Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": "Advertencia: ya hay una entrada en esta fecha. Si guarda, la entrada existente se sobrescribirá.",
  "@Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": {
    "type": "text",
    "placeholders": {}
  },
  "Date": "Fecha",
  "@Date": {
    "type": "text",
    "placeholders": {}
  },
  "change date": "cambiar fecha",
  "@change date": {
    "description": "Used as an action label on the button that triggers a calendar to select the date for an attack when a screen reader is used. It will be contextualized by the OS, on Android the OS will say the label of the button, then the word 'button', followed by 'double tap to change date'",
    "type": "text",
    "placeholders": {}
  },
  "Strength": "Intensidad",
  "@Strength": {
    "type": "text",
    "placeholders": {}
  },
  "Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": "El dolor es muy subjetivo. La única persona que sabe qué opción es la correcta eres tú. Si no está seguro, seleccione la más alta de las dos que está considerando.",
  "@Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": {
    "type": "text",
    "placeholders": {}
  },
  "Unable to perform most activities": "Con dificultad para realizar la mayoría de las actividades",
  "@Unable to perform most activities": {
    "description": "Migraine strength 3/3",
    "type": "text",
    "placeholders": {}
  },
  "Unable to perform some activities": "Con dificultad para realizar algunas de las actividades",
  "@Unable to perform some activities": {
    "description": "Migraine strength 2/3",
    "type": "text",
    "placeholders": {}
  },
  "Able to perform most activities": "Con capacidad para realizar la mayoría de las actividades",
  "@Able to perform most activities": {
    "description": "Migraine strength 1/3",
    "type": "text",
    "placeholders": {}
  },
  "Close": "Cerrar",
  "@Close": {
    "description": "Used as a 'close' button in the help dialog for migraine strength. This will be converted to upper case when used in this context, ie. 'Close' becomes 'CLOSE'. This to fit with Android UI guidelines",
    "type": "text",
    "placeholders": {}
  },
  "Which strength should I choose?": "¿Qué intensidad debo elegir?",
  "@Which strength should I choose?": {
    "description": "Used as the tooltip for the help button in the 'add' and 'edit' screens as well as the title of the help window that pops up when this button is pressed.",
    "type": "text",
    "placeholders": {}
  },
  "Medications taken": "Tomar medicamentos",
  "@Medications taken": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders": {}
  },
  "Note": "Nota",
  "@Note": {
    "type": "text",
    "placeholders": {}
  },
  "(none)": "(ninguno)",
  "@(none)": {
    "description": "Used in exported HTML when the user has taken no medications. It is an entry in a table where the header is 'Medications'",
    "type": "text",
    "placeholders": {}
  },
  "Medications": "Medicamentos",
  "@Medications": {
    "description": "Used as a table header in exported data",
    "type": "text",
    "placeholders": {}
  },
  "Migraine Log": "Registro de migraña",
  "@Migraine Log": {
    "type": "text",
    "placeholders": {}
  },
  "hide": "ocultar",
  "@hide": {
    "description": "Used in exported HTML as a link that hides one month. Will be displayed like '[hide]', so it should be lower case unless there are good reasons not to",
    "type": "text",
    "placeholders": {}
  },
  "Hide this month": "Ocultar este mes",
  "@Hide this month": {
    "description": "Tooltip for a link that hides a month from view in the exported HTML",
    "type": "text",
    "placeholders": {}
  },
  "hidden:": "ocultos:",
  "@hidden:": {
    "description": "Will be rendered like: \"hidden: january 2021\"",
    "type": "text",
    "placeholders": {}
  },
  "Total headache days": "Total de días con dolores de cabeza",
  "@Total headache days": {
    "description": "Used in a table, will render like 'Total headache days    20 days'",
    "type": "text",
    "placeholders": {}
  },
  "Taken medication": "Medicación tomada",
  "@Taken medication": {
    "description": "Used in a table, will render like 'Taken Medication    5 days'",
    "type": "text",
    "placeholders": {}
  },
  "Filter:": "Filtrar:",
  "@Filter:": {
    "description": "Will be joined with the string 'show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders": {}
  },
  "show": "mostrar",
  "@show": {
    "description": "Will be joined with the string 'Filter:' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders": {}
  },
  "everything": "todo",
  "@everything": {
    "description": "Will be joined with the string 'Filter: show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders": {}
  },
  "months": "meses",
  "@months": {
    "description": "Will be joined with a number, which will be 3 or higher, to become ie. '3 months'",
    "type": "text",
    "placeholders": {}
  },
  "_generatedMessage": "Generado por Registro de migraña versión {version}",
  "@_generatedMessage": {
    "type": "text",
    "placeholders": {
      "version": {}
    }
  },
  "Loading...": "Cargando...",
  "@Loading...": {
    "description": "Used in exported HTML to indicate that we're loading the exported data",
    "type": "text",
    "placeholders": {}
  },
  "Previous month": "Mes anterior",
  "@Previous month": {
    "description": "Tooltip for the previous month button",
    "type": "text",
    "placeholders": {}
  },
  "Next month": "Mes siguiente",
  "@Next month": {
    "description": "Tooltip for the next month button",
    "type": "text",
    "placeholders": {}
  },
  "_currentMonth": "Mes actual: {month}",
  "@_currentMonth": {
    "description": "Used for screen readers to describe the element in the month selector that indicates the current month. The month variable will contain a pre-localized month name along with the year",
    "type": "text",
    "placeholders": {
      "month": {}
    }
  },
  "Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": "El registro de migraña le ayuda a llevar un diario de dolores de cabeza. Una vez que haya añadido una entrada, esta pantalla será sustituida por las estadísticas.\n\nAñada una nueva entrada pulsando el botón \"+\".\n\nPara obtener más ayuda, seleccione \"Ayuda\" en el menú de la parte superior derecha de la pantalla.",
  "@Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": {
    "type": "text",
    "placeholders": {}
  },
  "Help": "Ayuda",
  "@Help": {
    "type": "text",
    "placeholders": {}
  },
  "Icons in the calendar": "Iconos en el calendario",
  "@Icons in the calendar": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders": {}
  },
  "Colours": "Colores",
  "@Colours": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders": {}
  },
  "An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": "Un subrayado debajo de un día en el calendario significa que hay una entrada registrada en esa fecha, pero que no se tomó ninguna medicación.",
  "@An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": {
    "type": "text",
    "placeholders": {}
  },
  "A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": "Un círculo alrededor de un día en el calendario significa que hay una entrada registrada en esa fecha, y que se tomó la medicación.",
  "@A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": {
    "type": "text",
    "placeholders": {}
  },
  "Througout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": "En todo el Registro de migraña se utilizan colores para indicar la intensidad de una migraña. El subrayado/círculo en el calendario, así como el texto de la intensidad al añadir o editar una entrada, utilizarán un color para significar la intensidad.",
  "@Througout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": {
    "type": "text",
    "placeholders": {}
  },
  "dayString": "{number,plural, =1{{number} día}=2{{number} días}other{{number} días}}",
  "@dayString": {
    "description": "How many days there are. This isn't as complicated as it looks. Unless you know you need to modify the pluralization rules (in which case see https://pub.dev/documentation/intl/latest/intl/Intl/plural.html) you should just change the string 'day' and 'days' here into your local equivalent. For instance, the Norwegian version of this string is: '{number,plural, =1{{number} dag}=2{{number} dagar}other{{number} dagar}}'. number will always be a positive integer (never zero).",
    "type": "text",
    "placeholders": {
      "number": {}
    }
  },
  "Successfully imported data": "Datos importados con éxito",
  "@Successfully imported data": {
    "type": "text",
    "placeholders": {}
  },
  "The file is corrupt and can not be imported": "El archivo está corrupto y no se puede importar",
  "@The file is corrupt and can not be imported": {
    "type": "text",
    "placeholders": {}
  },
  "This file does not appear to be a Migraine Log (html) file": "Este archivo no parece ser un archivo (html) del Registro de migraña",
  "@This file does not appear to be a Migraine Log (html) file": {
    "type": "text",
    "placeholders": {}
  },
  "This file is from a newer version of Migraine Log. Upgrade the app first.": "Este archivo es de una versión más reciente del Registro de migraña. Actualice primero la aplicación.",
  "@This file is from a newer version of Migraine Log. Upgrade the app first.": {
    "type": "text",
    "placeholders": {}
  },
  "An unknown error ocurred during import.": "Se ha producido un error desconocido durante la importación.",
  "@An unknown error ocurred during import.": {
    "type": "text",
    "placeholders": {}
  },
  "Import failed:": "Error de importación:",
  "@Import failed:": {
    "description": "Will contain information about why after the :",
    "type": "text",
    "placeholders": {}
  },
  "Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": "Su versión de Registro de migraña es demasiado antigua para leer los datos que tiene. Por favor, actualice Registro de migraña.",
  "@Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": {
    "type": "text",
    "placeholders": {}
  },
  "Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": "Registro de migraña es software libre: puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU publicada por la Fundación para el Software Libre, ya sea la versión 3 de la Licencia, o (a su elección) cualquier versión posterior.\n\nEste programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o ADECUACIÓN A UN PROPÓSITO PARTICULAR.",
  "@Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": {
    "type": "text",
    "placeholders": {}
  },
  "Copyright ©": "Copyright ©",
  "@Copyright ©": {
    "description": "Will be followed by the author name and copyright year, which will be a link",
    "type": "text",
    "placeholders": {}
  },
  "See the": "Véase la",
  "@See the": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders": {}
  },
  "for details.": "para más detalles.",
  "@for details.": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders": {}
  },
  "About": "Acerca de",
  "@About": {
    "description": "Menu entry for triggering the about dialog",
    "type": "text",
    "placeholders": {}
  },
  "Exit": "Salir",
  "@Exit": {
    "description": "Used to exit the app",
    "type": "text",
    "placeholders": {}
  },
  "Export": "Exportar",
  "@Export": {
    "type": "text",
    "placeholders": {}
  },
  "Import": "Importar",
  "@Import": {
    "type": "text",
    "placeholders": {}
  },
  "Statistics": "Estadísticas",
  "@Statistics": {
    "description": "Tooltip for the statistics tab",
    "type": "text",
    "placeholders": {}
  },
  "Calendar": "Calendario",
  "@Calendar": {
    "description": "Tooltip for the calendar tab",
    "type": "text",
    "placeholders": {}
  },
  "Home": "Inicio",
  "@Home": {
    "description": "Tooltip for the home tab",
    "type": "text",
    "placeholders": {}
  },
  "Translated by": "Traducido por",
  "@Translated by": {
    "description": "Should be a literal translation of this phrase, the value of TRANSLATED_BY will be appended to make a string like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders": {}
  },
  "TRANSLATED_BY": "Diego",
  "@TRANSLATED_BY": {
    "description": "Should be an alphabetical list of the names of the translators of this language, delimited with commas or the localized equivalent of \"and\". Will be displayed in the about dialog like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders": {}
  },
  "deletedDateMessage": "Borrado {fmtDate}",
  "@deletedDateMessage": {
    "type": "text",
    "placeholders": {
      "fmtDate": {}
    }
  },
  "Delete": "Borrar",
  "@Delete": {
    "type": "text",
    "placeholders": {}
  },
  "Add": "Añadir",
  "@Add": {
    "type": "text",
    "placeholders": {}
  },
  "Show action menu": "Mostrar menú de acciones",
  "@Show action menu": {
    "type": "text",
    "placeholders": {}
  },
  "lastNDaysMessage": "Últimos {days} días",
  "@lastNDaysMessage": {
    "description": "days should always be >1, so plural",
    "type": "text",
    "placeholders": {
      "days": {}
    }
  },
  "took no medication": "no tomó medicamentos",
  "@took no medication": {
    "type": "text",
    "placeholders": {}
  },
  "tookMedsMessage": "tomó {meds}",
  "@tookMedsMessage": {
    "description": "This will expand into a list of medications taken, and will be added to the type of headache the user had. For instance, this might get 'Imigran' as meds, this will then be 'took Imigran', and it might expand into the sentece 'Migraine, took Imigran'",
    "type": "text",
    "placeholders": {
      "meds": {}
    }
  },
  "Note:": "Nota:",
  "@Note:": {
    "type": "text",
    "placeholders": {}
  },
  "Show note indicators": "Mostrar indicadores de nota",
  "@Show note indicators": {
    "description": "Toggle button that enables/disables showing a marker on dates with notes in the calendar",
    "type": "text",
    "placeholders": {}
  },
  "Show a small dot on dates where you have written a note": "Muestra un pequeño punto en las fechas que ha escrito una nota",
  "@Show a small dot on dates where you have written a note": {
    "description": "The 'dot' is a small grey square, displayed in the upper right hand corner of the date number in the calendar",
    "type": "text",
    "placeholders": {}
  },
  "List days with no entries": "Listar días sin entradas",
  "@List days with no entries": {
    "description": "Checkbox, toggles showing or hiding (default=hide) dates that have no registered headaches in the tables in our exported HTML file",
    "type": "text",
    "placeholders": {}
  },
  "A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": "Un punto en la esquina superior derecha de la fecha indica hay una nota junto a la entrada de esa fecha.",
  "@A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": {
    "type": "text",
    "placeholders": {}
  },
  "Aura only": "Solo aura",
  "@Aura only": {
    "description": "Used as the 'strength' of a migraine or headache where the user only had aura symptoms",
    "type": "text",
    "placeholders": {}
  },
  "Aura symptoms only": "Solo síntomas del aura",
  "@Aura symptoms only": {
    "description": "Migraine strength help text for aura only",
    "type": "text",
    "placeholders": {}
  },
  "Let me set \"aura only\" as a the strength of an attack": "Permítame establecer \"solo aura\" como intensidad del ataque",
  "@Let me set \"aura only\" as a the strength of an attack": {
    "description": "This is used as a description for the 'aura only' config option. Feel free to rephrase.",
    "type": "text",
    "placeholders": {}
  },
  "Enable \"aura only\" strength": "Activar como intensidad \"solo aura\"",
  "@Enable \"aura only\" strength": {
    "description": "Toggle button that enables (or disables) use of \"aura only\" as a \"strength\" option",
    "type": "text",
    "placeholders": {}
  }
}
