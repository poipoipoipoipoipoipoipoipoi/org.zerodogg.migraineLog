// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:MigraineLog/helpwelcome.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:provider/provider.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:platform/platform.dart';
import 'utils.dart';

void main() {
  var skipGoldenTests;
  if (!LocalPlatform().isLinux) {
    skipGoldenTests = "Golden tests are only run on Linux";
  }

  TestWidgetsFlutterBinding.ensureInitialized();
  setTestFile('helpwelcome');

  testWidgets('MigraineLogWelcome', (WidgetTester tester) async {
    await goldenTest(
      widgetType: MigraineLogWelcome,
      widgetInstance: MigraineLogWelcome(),
      tester: tester,
    );
  }, skip: skipGoldenTests);
  testWidgets('MigraineLogHelp', (WidgetTester tester) async {
    var c = MigraineLogConfig();
    await tester.pumpWidget(materialWidgetBuilder(
      (context) => MigraineLogHelp(),
      provide: [
        ChangeNotifierProvider.value(value: c),
      ],
    ));
    await goldenTest(
      widgetType: MigraineLogHelp,
      name: 'MigraineLogHelp',
      tester: tester,
    );
  }, skip: skipGoldenTests);
  testWidgets('MigraineLogHelp with noteMarker', (WidgetTester tester) async {
    var c = MigraineLogConfig();
    c.displayNoteMarker = true;
    await tester.pumpWidget(materialWidgetBuilder(
      (context) => MigraineLogHelp(),
      provide: [
        ChangeNotifierProvider.value(value: c),
      ],
    ));
    await goldenTest(
      widgetType: MigraineLogHelp,
      name: 'MigraineLogHelpWNoteMarker',
      tester: tester,
    );
    await tester.flush();
  }, skip: skipGoldenTests);
}
